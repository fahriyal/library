package id.co.firzil.cobalibrary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class AmbilGambar {
	private static final String FILE_FORMAT = "yyyyMMddHHmmss";
	private Context c;
	private Fragment fragment;
	private Uri image_uri;
	private String direktori_image;
	public static final int PICK_FROM_CAMERA = 1, PICK_FROM_FILE = 2;

	public AmbilGambar(Context c, String direktori_image){
		this.c = c;
		this.direktori_image = direktori_image;
	}

	public AmbilGambar(Context c, String direktori_image, Fragment f){
		this.fragment = f;
		this.c = c;
		this.direktori_image = direktori_image;
	}
	
	public void showDialogChooser(){
		AlertDialog.Builder dialog = new AlertDialog.Builder(c);
		dialog.setTitle("Ambil gambar");
		dialog.setPositiveButton("Kamera", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				startCamera();
			}
		});
		dialog.setNegativeButton("File", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				startFileChooser();
			}
		});
		dialog.show();
	}
	
	public void startFileChooser(){
		Intent i = Intent.createChooser(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), "Ambil menggunakan");
		if(fragment != null) fragment.startActivityForResult(i, PICK_FROM_FILE);
		else ((Activity)c).startActivityForResult(i, PICK_FROM_FILE);
	}

	public void startCamera(){
		Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    	
		if(i.resolveActivity(c.getPackageManager()) != null){
			String image_path = direktori_image + new SimpleDateFormat(FILE_FORMAT).format(new Date()) + ".jpg";
			File f = new File(image_path);
			image_uri = Uri.fromFile(f);
			i.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);

			if(fragment != null) fragment.startActivityForResult(i, PICK_FROM_CAMERA);
			else ((Activity) c).startActivityForResult(i, PICK_FROM_CAMERA);
		}
		else Toast.makeText(c, "Tidak ada aplikasi kamera yang terinstall", Toast.LENGTH_SHORT).show();
	}
	
	//function ini dipanggil di dalam method onActivityResult
	public String processResultAndReturnImagePath(int requestCode, Intent data){
		String image_path = null;
		if(requestCode == PICK_FROM_FILE){
			Uri selectedImage = data.getData();
		    String[] filePathColumn = { MediaStore.Images.Media.DATA };
			Cursor cursor = c.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
		    cursor.moveToFirst();
		    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		    image_path = cursor.getString(columnIndex);
		    cursor.close();
		}
		else if(requestCode == PICK_FROM_CAMERA) image_path = image_uri.getPath();
		
		return image_path;
	}
}