package id.co.firzil.cobalibrary;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

public class SessionVersionChecker {

	private static final String
			MUST_UPDATE = "must_update",
			IS_CHECKED_VERSION = "is_checked_version",
			URL_UPDATE = "url_update",
			IS_UPDATED_VERSION = "is_updated_version";
	public static final String PLAYSTORE = "playstore";

	private Context context;

	public SessionVersionChecker(Context c){
		context = c;
	}
	
	private SharedPreferences getSharedPreferences() {
		return context.getSharedPreferences("version_checker_prefs", Context.MODE_PRIVATE);
	}
	
	private void commit(String key, String value) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	private String getValue(String key) {
		return getSharedPreferences().getString(key, "");
	}

	public void setMustUpdate(boolean must_update) {
		commit(MUST_UPDATE, must_update ? "1" : "");
	}

	public boolean isMustUpdate(){
		return ! TextUtils.isEmpty(getValue(MUST_UPDATE));
	}

	public void setIsSudahNgeCekVersi(boolean sudah) {
		commit(IS_CHECKED_VERSION, sudah ? "1" : "");
	}

	public boolean isBelumNgecekVersi(){
		return TextUtils.isEmpty(getValue(IS_CHECKED_VERSION));
	}

	public void setUrlUpdate(String v) {
		commit(URL_UPDATE, v);
	}

	public String getUrlUpdate(){
		return getValue(URL_UPDATE);
	}

	public void setIsVersiTerbaru(boolean versi_terbaru){
		commit(IS_UPDATED_VERSION, versi_terbaru ? "1" : "");
	}

	public boolean isVersiTerbaru(){
		return ! TextUtils.isEmpty(getValue(IS_UPDATED_VERSION));
	}

}
