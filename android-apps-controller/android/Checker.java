package id.co.hokben.kelas;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.net.URL;
import java.util.Date;

/*
 * Created by fahriyalafif on 7/8/2015.
 */
public class Checker {
    private Context c;
    private Dialog dialog_session, dialog_version;
    private SessionChecker svc;
    private OnCloseClickListener onCloseClick;

    public interface OnCloseClickListener{
        void onClose();
    }

    public Checker(Context c){
        this.c = c;
        svc = new SessionChecker(c);
    }

    public void setOnCloseClickListener(OnCloseClickListener onCloseClick){
        this.onCloseClick = onCloseClick;
    }

    private int dpToPixel(int dp) {
        float scale = c.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    private void showDialogOffApp(){
        try{
            if(dialog_session == null){
                dialog_session = new Dialog(c);

                LinearLayout l = new LinearLayout(c);
                l.setBackgroundColor(Color.WHITE);
                l.setOrientation(LinearLayout.VERTICAL);

                int padding = dpToPixel(10);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                l.setLayoutParams(lp);
                l.setPadding(padding, padding, padding, padding);

                TextView judul = new TextView(c);
                judul.setText("Session Checker");
                judul.setTextColor(Color.parseColor("#511f1f"));
                judul.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                judul.setGravity(Gravity.CENTER);
                judul.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                l.addView(judul);

                LinearLayout.LayoutParams hm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                hm.setMargins(0, padding, 0, padding);

                TextView msg = new TextView(c);
                msg.setText(svc.getSessionMessage());
                msg.setTextColor(Color.BLACK);
                msg.setLayoutParams(hm);
                msg.setGravity(Gravity.CENTER);
                msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                l.addView(msg);

                LinearLayout.LayoutParams button_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                button_param.gravity = Gravity.CENTER_HORIZONTAL;

                Button ok = new Button(c);
                ok.setText("Close");
                ok.setTextColor(Color.WHITE);
                ok.setBackgroundColor(Color.parseColor("#3bafda"));
                ok.setLayoutParams(button_param);
                ok.setGravity(Gravity.CENTER);
                ok.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog();
                        if(onCloseClick != null) onCloseClick.onClose();
                    }
                });

                l.addView(ok);

                dialog_session.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog_session.setContentView(l);
                dialog_session.setCanceledOnTouchOutside(false);
                dialog_session.setCancelable(false);

                dialog_session.setTitle("");
            }

            if(! dialog_session.isShowing()) dialog_session.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void cekSession(){
        try {
            if(isOnLine() && (svc.isBelumNgecekSession() || svc.isSudah30menit())){
                new AsyncTask<String, String, String>(){

                    private JSONObject j;

                    @Override
                    protected String doInBackground(String... strings) {
                        try{
                            URL url = new URL("http://versioncheck.mlogg.com/index.php/api/version/check/"+c.getPackageName()+"/?mobile_version="+getAppVersion());
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setReadTimeout(15000);
                            conn.setConnectTimeout(15000);
                            conn.setRequestMethod("GET");
                            conn.connect();

                            InputStream in = conn.getInputStream();

                            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf-8"), 8);
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while ((line = reader.readLine()) != null) {
                                sb.append(line).append("\n");
                            }
                            in.close();

                            String json = sb.toString();

                            Log.d("CHECKER RESPONSE", "CHECKER RESPONSE "+json);

                            j = new JSONObject(json);
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }
                        return null;
                    }

                    protected void onPostExecute(String result){
                        super.onPostExecute(result);
                        if (j != null) {
                            try {
                                if (j.optInt("status_code") == 1) {
                                    svc.setLastTimeCheck(new Date().getTime());
                                    svc.setSessionStatus(j.optString("session_status"));
                                    svc.setSessionMessage(j.optString("session_message"));
                                    svc.setIsSudahNgeCekSession(true);

                                    boolean hatus_update = j.optBoolean("status_mandatory");
                                    String endpoint = j.optString("end_point");

                                    JSONObject status_app = j.getJSONObject("status_app");
                                    boolean is_terbaru = status_app.optBoolean("is_terbaru");
                                    String url_update = status_app.optString("url_update");

                                    svc.setIsVersiTerbaru(is_terbaru);
                                    svc.setMustUpdate(hatus_update);
                                    svc.setUrlUpdate(url_update);
                                    svc.setIsSudahNgeCekVersi(true);
                                    svc.setEndPointApi(endpoint);

                                    if (! svc.isSessionStatusOn()) showDialogOffApp();
                                    else if (!svc.isVersiTerbaru()) showDialogUpdate();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
            else if(! svc.isSessionStatusOn()) showDialogOffApp();
            else if(! svc.isVersiTerbaru()) showDialogUpdate();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showDialogUpdate(){
        try{
            if(dialog_version == null){
                dialog_version = new Dialog(c);

                LinearLayout l = new LinearLayout(c);
                l.setOrientation(LinearLayout.VERTICAL);
                l.setBackgroundColor(Color.WHITE);

                int padding = dpToPixel(10);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                l.setLayoutParams(lp);
                l.setPadding(padding, padding, padding, padding);

                TextView judul = new TextView(c);
                judul.setText("Version Checker");
                judul.setTextColor(Color.parseColor("#511f1f"));
                judul.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                judul.setGravity(Gravity.CENTER);
                judul.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                l.addView(judul);

                LinearLayout.LayoutParams hm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                hm.setMargins(0, padding, 0, padding);

                TextView msg = new TextView(c);
                msg.setText("Update version has been released. Update now!");
                msg.setTextColor(Color.BLACK);
                msg.setLayoutParams(hm);
                msg.setGravity(Gravity.CENTER);
                msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                l.addView(msg);

                LinearLayout.LayoutParams button_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                button_param.gravity = Gravity.CENTER_HORIZONTAL;

                Button ok = new Button(c);
                ok.setText("Update");
                ok.setTextColor(Color.WHITE);
                ok.setBackgroundColor(Color.parseColor("#3bafda"));
                ok.setLayoutParams(button_param);
                ok.setGravity(Gravity.CENTER);
                ok.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = svc.getUrlUpdate();
                        if (url.equalsIgnoreCase(SessionChecker.PLAYSTORE)) {
                            String appPackageName = c.getPackageName();
                            try {
                                c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                dismissDialog();
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(c, "No Play Store Application Installed", Toast.LENGTH_LONG).show();
                                loadUrlApk("http://play.google.com/store/apps/details?id=" + appPackageName);
                            }
                        }
                        else loadUrlApk(url);
                    }
                });

                l.addView(ok);

                dialog_version.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog_version.setContentView(l);
                dialog_version.setCanceledOnTouchOutside(false);

                dialog_version.setTitle("");
            }
            dialog_version.setCancelable(! svc.isMustUpdate());
            if(! dialog_version.isShowing()) dialog_version.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadUrlApk(String url_apk){
        try {
            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url_apk)));
            dismissDialog();
        }
        catch (android.content.ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            Toast.makeText(c, anfe.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void dismissDialog(){
        if(dialog_session != null && dialog_session.isShowing()) dialog_session.dismiss();
        if(dialog_version != null && dialog_version.isShowing()) dialog_version.dismiss();
    }

    private boolean isOnLine(){
        ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private String getAppVersion(){
        try {
            return c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "1.0.0";
        }
    }

}