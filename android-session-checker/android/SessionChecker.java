package id.co.firzil.cobalibrary;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import java.util.Calendar;

public class SessionChecker {

	private static final String
			SESSION_MESSAGE = "session_message",
			SESSION_STATUS = "session_status",
			IS_CHECKED_SESSION = "is_checked_session",
			SESSION_STATUS_ON = "on",
			LAST_TIME_CHECK = "last_time_check";

	private Context context;

	public SessionChecker(Context c){
		context = c;
	}
	
	private SharedPreferences getSharedPreferences() {
		return context.getSharedPreferences("session_checker_prefs", Context.MODE_PRIVATE);
	}
	
	private void commit(String key, String value) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	private String getValue(String key) {
		return getSharedPreferences().getString(key, "");
	}

	public void setSessionStatus(String sessionStatus) {
		commit(SESSION_STATUS, sessionStatus);
	}

	public boolean isSessionStatusOn(){
		return getValue(SESSION_STATUS).equalsIgnoreCase(SESSION_STATUS_ON);
	}

	public void setIsSudahNgeCekSession(boolean sudah) {
		commit(IS_CHECKED_SESSION, sudah ? "1" : "");
	}

	public boolean isBelumNgecekSession(){
		return TextUtils.isEmpty(getValue(IS_CHECKED_SESSION));
	}

	public void setSessionMessage(String sessionMessage) {
		commit(SESSION_MESSAGE, sessionMessage);
	}

	public String getSessionMessage(){
		return getValue(SESSION_MESSAGE);
	}

	public void setLastTimeCheck(long lastTimeCheck) {
		commit(LAST_TIME_CHECK, lastTimeCheck+"");
	}

	public long getLastTimeCheck(){
		String waktu = getValue(LAST_TIME_CHECK);
		if(TextUtils.isEmpty(waktu)) return 0;
		else return Long.parseLong(waktu);
	}

	public boolean isSudah30menit(){
		final int SECOND_MILLIS = 1000;
		final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
		long time = getLastTimeCheck();
		long now = Calendar.getInstance().getTimeInMillis();

		final long diff = now - time;
		boolean sudah_30_menit = true;
		if(diff < 30 * MINUTE_MILLIS) sudah_30_menit = false;

		//Log.d("waktu", "waktu = "+sudah_30_menit+" last = "+time+" now = "+now);

		return sudah_30_menit;
	}

}