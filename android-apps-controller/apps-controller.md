# README #

Ini adalah library untuk mengecek versi terbaru yang ada di server dengan yg sedang berjalan di mobile.

Fitur:
- Memberikan informasi aplikasi apakah up to date atau tidak
- Admin bisa mematikan aplikasi dari backend

Jika versi aplikasi di mobile sama dengan yang di server maka tidak terjadi apa2

Jika versi aplikasi di mobile berbeda dengan yg di server maka ada 2 aksi
- jika mandatory maka aplikasi di mobile WAJIB di update
- jika non-mandatory, maka aplikasi di mobile BOLEH tidak di update

Ada response url_update di server yg menunjukkan url untuk update aplikasi (playstore / http://abc.com)

### Persiapan ###

* Download file library untuk [android](android)

### Cara instalasi ###

- Ubah package di file [android](android) sesuai dengan package aplikasimu
- Di manifest aplikasi tambahkan permission
```
#!xml
	<uses-permission android:name="android.permission.INTERNET" /> 
	<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
```

- Di activitymu yg awal (misal splash screen)di oncreate tambahkan script
```
#!java
	SessionChecker svn = new SessionChecker(c);
	svn.setIsSudahNgeCekSession(false);   //saat membuka halaman awal diset belum ngecek ke server
	svn.setLastTimeCheck(0);
```

- override method onResume dan onDestroy di semua activitymu, dengan menambahkan script

```
#!java
	private Checker checker;

    public void onResume() {
        super.onResume();

        if (checker == null) {
			checker = new Checker(this);
			checker.setOnCloseClickListener(new Checker.OnCloseClickListener() {   //set listener ketika tombol close dialog di klik
				@Override
				public void onClose() {
					finish();
				}
			});
		}
		checker.cekSession();   //lalu ngecek
    }

    public void onDestroy() {
        super.onDestroy();
        if (checker != null) checker.dismissDialog();   //jika activity nutup maka dialog ditutup
    }
```

- lalu untuk mengubah versi yg terbaru di backend [http://versioncheck.mlogg.com/](http://versioncheck.mlogg.com/) bisa kamu ubah sesuka hati

- Have Fun