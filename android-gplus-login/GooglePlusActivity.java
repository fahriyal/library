package id.co.firzil.nagcoba;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import android.app.ProgressDialog;
import android.support.v4.app.FragmentActivity;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public abstract class GooglePlusActivity extends FragmentActivity implements ConnectionCallbacks, OnConnectionFailedListener {

    public interface OnConnectedGPlus{
        void onConnected(Person person, String email);
    }

    private OnConnectedGPlus onConnectedGPlus;
    private static final int RC_SIGN_IN = 0;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog pd;

    /**
     * A flag indicating that a PendingIntent is in progress and prevents us
     * from starting further intents.
     */
    private boolean mIntentInProgress;

    private boolean mSignInClicked;

    protected void setOnConnectedGPlus(OnConnectedGPlus onConnectedGPlus){
        this.onConnectedGPlus = onConnectedGPlus;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Please wait...");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    protected void onDestroy(){
        super.onDestroy();
        if (isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            //mGoogleApiClient.disconnect();
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
        }
    }

    private void connect(){
        showPD();
        if(! mGoogleApiClient.isConnecting()) {
            Log.d("GPLUS", "GPLUS CONNECTING");
            mGoogleApiClient.connect();
        }
        else Log.d("GPLUS", "GPLUS TIDAK SEDANG KONEK");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d("GPLUS", "GPLUS ON CONNECTION FAILED");
        dismissPD();

        if (! result.hasResolution()) {
            Log.d("GPLUS", "GPLUS NO RESOLUTION");
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
        }
        else if (! mIntentInProgress) {
            Log.d("GPLUS", "GPLUS MANTEPPP");
            if (mSignInClicked) {
                if (result != null && result.hasResolution()) {
                    try {
                        mIntentInProgress = true;
                        result.startResolutionForResult(this, RC_SIGN_IN);
                    } catch (SendIntentException e) {
                        mIntentInProgress = false;
                        connect();
                    }
                }
                else connect();
            }
            else Log.d("GPLUS", "GPLUS SIGN CLICKED FALSE");
        }
        else Log.d("GPLUS", "GPLUS INTENT PROGRESS TRUE");

    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        Log.d("GPLUS", "GPLUS ON ACTIVITY RESULT = " + responseCode);

        if (requestCode == RC_SIGN_IN) {
            mIntentInProgress = false;
            if(responseCode == RESULT_OK) {
                mSignInClicked = true;
                connect();
            }
            else mSignInClicked = false;
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        Log.d("GPLUS", "GPLUS ON CONNECTED");
        mSignInClicked = false;

        dismissPD();
        try {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            if (currentPerson != null) {
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                if(onConnectedGPlus != null) onConnectedGPlus.onConnected(currentPerson, email);
            }
            else {
                Toast.makeText(getApplicationContext(), "Person information is null", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Exception occured", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        connect();
    }

    /**
     * Sign-in into google
     * */
    protected void signInWithGplus() {
        mSignInClicked = true;
        if(isConnected()) {
            Log.d("GPLUS", "GPLUS ISCONNECTED");
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.e("GPLUS", "GPLUS User access revoked!");
                            mGoogleApiClient.disconnect();
                            connect();
                        }

                    });
        }
        else connect();

        showPD();
    }

    private boolean isConnected(){
        return mGoogleApiClient.isConnected();
    }

    private void showPD(){
        if(! pd.isShowing())pd.show();
    }

    private void dismissPD(){
        if(pd.isShowing())pd.dismiss();
    }

}