# README #

Ini adalah library untuk mengambil gambar dari galeri atau kamera

### Persiapan ###

* Download file library untuk [android](android)
* Download file [contoh](example) agar mudah memahami


### Cara instalasi ###

- Ubah package di file [android](android) sesuai dengan package aplikasimu
- Di manifest aplikasi tambahkan permission
```
#!xml
	<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
```

- buat instance variabel dengan tipe data Ambil Gambar

```
#!java
	private AmbilGambar ambil_gambar;
```

- untuk menampilkan dialog ambil gambar dari kamera atau galeri panggil method ini

```
#!java
	if(ambil_gambar == null) {
	    ambil_gambar = new AmbilGambar(context, folder_gambar); //kalo manggilnya di activity
	    //ambil_gambar = new AmbilGambar(context, folder_gambar, fragment);  //kalo manggilnya di fragment
	}
	ambil_gambar.showDialogChooser();
```

- Di activitymu override method onActivityResult

```
#!java
	@Override
    public void onActivityResult(int request, int result, Intent data) {
        super.onActivityResult(request, result, data);
        if(result == Activity.RESULT_OK){
            if(request == AmbilGambar.PICK_FROM_CAMERA || request == AmbilGambar.PICK_FROM_FILE){
				//disini memproses hasil gambar
				String path_asli = ambil_gambar.processResultAndReturnImagePath(request, data);
				gambar.setImageURI(Uri.parse(path_asli));
			}
		}
```

- Have Fun