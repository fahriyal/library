# README #

Ini adalah library untuk mengecek session aplikasi yang ada di server. aplikasi akan mengecek ke server dalam rentang minimal 30 menit sekali.

- Jika session di server off maka aplikasi mobile tidak bisa digunakan
- Jika session di server on maka aplikasi mobile bisa digunakan

dengan menggunakan cara ini developer php bisa mematikan / menghidupkan aplikasi serta memberikan pesan alasan aplikasi dimatikan seperti "maaf aplikasi tidak bisa digunakan karena ada maintenance di server" atau "maaf aplikasi tidak bisa digunakan karena klien belum membayar tagihan :DDD"

Usahakan struktur response api [session_checking.php](php) tetap seperti itu

### Persiapan ###

* Download file library untuk [android](android) dan [php](php)
* Download file [contoh](example) agar mudah memahami


### Cara instalasi ###

- Ubah package di file [android](android) sesuai dengan package aplikasimu
- Di manifest aplikasi tambahkan permission
```
#!xml
	<uses-permission android:name="android.permission.INTERNET" /> 
	<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
```

- Di activitymu yg awal (misal splash screen)di oncreate tambahkan script
```
#!java
	SessionChecker svn = new SessionChecker(this);
    svn.setIsSudahNgeCekSession(false);   //saat membuka halaman awal diset belum ngecek ke server
```

- override method onResume dan onDestroy di semua activitymu, dengan menambahkan script

```
#!java
	private Checker u;
    public static final String URL_SESSION_CHECKER = "http://10.0.2.2/lat-gcm/session_checking.php";  //localhost untuk android

    public void onResume() {
        super.onResume();

        if (u == null) {
            u = new Checker(this);
            u.setOnCloseClickListener(new Checker.OnCloseClickListener() {   //set listener ketika tombol close dialog di klik
                @Override
                public void onClose() {
                    finish();
                }
            });
            u.setUrlApiSessionChecker(URL_SESSION_CHECKER);   //set url api untuk ngecek versi app yg ada diserver
        }
        u.cekSession();   //lalu ngecek
    }

    public void onDestroy() {
        super.onDestroy();
        if (u != null) u.dismissDialog();   //jika activity nutup maka dialog ditutup
    }
```
- taruh file [php](php) di server mu
- lalu untuk mengubah status on atau off di file [session_checking.php](php/session_checking.php) di bagian $session_status

- Have Fun